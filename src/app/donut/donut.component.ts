import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
@Component({
  selector: 'app-donut',
  templateUrl: './donut.component.html',
  styleUrls: ['./donut.component.scss']
})
export class DonutComponent implements OnInit {

  data: any = [
    { name: 'Shift A', count: 35, color: 'rgb(31, 119, 180)' },
    // { name: 'AA', count: 0, color: '#FF8200' },
    // { name: 'A', count: 0, color: '#FFB700' },
    { name: 'shift B', count: 30, color: 'rgb(174, 199, 232)' },
    { name: 'shift C', count: 35, color: 'rgb(255, 127, 14)' },
  ];
  totalCount = 5;		//calcuting total manually
  total = "Total";
  parameter = "Parameters";
  width = 400;
  height = 600;
  radius = 150;
  constructor() { }
  ngOnInit(): void {
    this.drawdonut();
  }
  drawdonut() {


    // let color = d3.scaleOrdinal()
    //   .range(["#B4DC70", "#FEFE2B", "#FE8E2B", "#FE2B2B", "#2B5EFE"]);

    let arc: any = d3.arc()
      .outerRadius(this.radius - 5)
      .innerRadius(100);
    var pie = d3.pie()
      .sort(null)
      .value(function (d: any) {
        return d.count;
      });
    var svg = d3.select("#donut").append("svg")
      .attr("width", this.width)
      .attr("height", this.height)
      .append("g")
      .attr("transform", "translate(" + this.width / 2 + "," + this.height / 4 + ")");
    var g = svg.selectAll(".arc")
      .data(pie(this.data))
      .enter().append("g");
    g.append("path")
      .attr("d", arc)
      .style("fill", function (d: any, i) {
        return d.data.color;
      });
    // var textG = svg.selectAll(".labels")
    //   .data(pie(this.data))
    //   .enter().append("g")
    //   .attr("class", "labels");
    // Append text labels to each arc
    // textG.append("text")
    //   .attr("transform", function (d: any) {
    //     return "translate(" + arc.centroid(d) + ")";
    //   })
    //   .attr("dy", ".35em")
    //   .style("text-anchor", "middle")
    //   .attr("fill", "#fff")
    //   .text(function (d: any, i) {
    //     return d.data.count > 0 ? d.data.name : '';
    //   });
    // let outerArc = d3
    //   .arc()
    //   .innerRadius(this.radius * 0.9)
    //   .outerRadius(this.radius * 0.9);

    // svg
    //   .selectAll("allPolylines")
    //   .data(pie(this.data))
    //   .enter()
    //   .append("polyline")
    //   .attr("stroke", "black")
    //   .style("fill", "none")
    //   .attr("stroke-width", 1)
    //   .attr("points", (d: any) => {
    //     var posA = arc.centroid(d); // line insertion in the slice
    //     var posB = outerArc.centroid(d); // line break: we use the other arc generator that has been built only for that
    //     var posC = outerArc.centroid(d); // Label position = almost the same as posB
    //     var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2; // we need the angle to see if the X position will be at the extreme right or extreme left
    //     posC[0] = this.radius * 0.95 * (midangle < Math.PI ? 1 : -1); // multiply by 1 or -1 to put it on the right or on the left
    //     return [posA, posB, posC];
    //   });

    // Add the polylines between chart and labels:
    // svg
    //   .selectAll("allLabels")
    //   .data(pie(this.data))
    //   .enter()
    //   .append("text")
    //   .text((d: any) => {
    //     return d.data.name;
    //   })
    //   .attr("transform", (d: any) => {
    //     var pos = outerArc.centroid(d);
    //     var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;
    //     pos[0] = this.radius * 0.99 * (midangle < Math.PI ? 1 : -1);
    //     return "translate(" + pos + ")";
    //   })
    //   .style("text-anchor", (d: any) => {
    //     var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;
    //     return midangle < Math.PI ? "start" : "end";
    //   });
    // var legendG = svg.selectAll(".legend") // note appending it to mySvg and not svg to make positioning easier
    //   .data(pie(data))
    //   .enter().append("g")
    //   .attr("transform", function (d, i) {
    //     return "translate(" + (width - 110) + "," + (i * 15 + 20) + ")"; // place each legend on the right and bump each one down 15 pixels
    //   })
    //   .attr("class", "legend");
    // legendG.append("rect") // make a matching color rect
    //   .attr("width", 10)
    //   .attr("height", 10)
    //   .attr("fill", function (d, i) {
    //     return d.data.color;
    //   });
    // legendG.append("text") // add the text
    //   .text(function (d) {
    //     return d.value + "  " + d.data.name;
    //   })
    //   .style("font-size", 12)
    //   .attr("y", 10)
    //   .attr("x", 11);
    // g.append("text")
    //   .attr("transform", function (d) {
    //     var _d = arc.centroid(d);
    //     _d[0] *= 1.5;	//multiply by a constant factor
    //     _d[1] *= 1.5;	//multiply by a constant factor
    //     return "translate(" + _d + ")";
    //   })
    //   .attr("dy", ".50em")
    //   .style("text-anchor", "middle")
    //   .text(function (d) {
    //     if (d.data.percentage < 8) {
    //       return '';
    //     }
    //     return d.data.percentage + '%';
    //   });
    // g.append("text")
    //   .attr("text-anchor", "middle")
    //   .attr('font-size', '1.5em')
    //   .attr('y', -20)
    //   .text(this.totalCount)
    //   .style('fill', 'rgb(0,74,118)');
    // g.append("text")
    //   .attr("text-anchor", "middle")
    //   .attr('font-size', '1.5em')
    //   .attr('y', 10)
    //   .style('fill', '#A2B2BA')
    //   .text(this.total);
    // g.append("text")
    //   .attr("text-anchor", "middle")
    //   .attr('font-size', '1.5em')
    //   .attr('y', 40)
    //   .text(this.parameter)
    //   .style('fill', '#A2B2BA');
  }


}
