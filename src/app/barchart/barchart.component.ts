import { Component, Input, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss']
})
export class BarchartComponent implements OnInit {
  @Input() graphIndex?: number;
  @Input() data: any;
  // data = [
  //   { "Framework": "< Safety stock", "Stars": "90", "Released": "2014", "color": 'red' },
  //   { "Framework": "Over stock", "Stars": "48", "Released": "2013", "color": 'steelblue' },
  //   { "Framework": "< Reorder Point", "Stars": "45", "Released": "2016", "color": 'yellow' },
  //   { "Framework": "< Max stock", "Stars": "18", "Released": "2010", "color": 'green' },
  //   { "Framework": "< Min stock", "Stars": "42", "Released": "2011", "color": 'brown' },
  // ];
  svg: any;
  margin = 50;
  width = 450 - (this.margin * 2);
  height = 300 - (this.margin * 2);
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.createSvg();
      this.drawBars();
    }, 100);

  }
  createSvg() {
    d3.select(`#bar_${this.graphIndex}`).remove(); // To remove rendered items

    // Create element for graph
    const histogramContainer = document.querySelector('#bar_container_' + this.graphIndex);
    let graphEle = document.createElement('div');
    graphEle.id = 'bar_' + this.graphIndex;
    if (histogramContainer) {
      histogramContainer.appendChild(graphEle);
    }
    this.svg = d3.select(`#bar_${this.graphIndex}`)
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }
  drawBars() {
    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width + 200])
      .domain(this.data.map((d: any) => d.Framework))
      .padding(0.2);
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x).tickSize(0));
    // Draw the X-axis on the DOM
    // this.svg.append("g")
    //   .attr("transform", "translate(0," + this.height + ")")
    //   .call(d3.axisBottom(x))
    //   .selectAll("text")
    //   .attr("transform", "translate(-10,0)")
    //   .style("text-anchor", "end");

    var minvalue: any = d3.min(this.data, function (d: any) { return +d.Stars })
    var maxvalue: any = d3.max(this.data, function (d: any) { return +d.Stars })
    // Create the Y-axis band scale
    const y = d3.scaleLinear()
      .domain([0, maxvalue])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(this.data)
      .enter()
      .append("rect")
      .attr("x", (d: any) => x(d.Framework))
      .attr("y", (d: any) => y(d.Stars))
      .attr("width", x.bandwidth())
      .attr("height", (d: any) => this.height - y(d.Stars))
      .attr('fill', 'green')
    // .style("fill", function (d: any, i: any) {
    //   return d.data.color;
    // })
    // .attr("fill", (d: any) => {
    //   return d.color;
    // });


    // this.svg.append("g")
    //   .selectAll("rect")
    //   .data(this.data)
    //   .join("rect")
    //   .attr("x", (d: any) => x(d.Framework) + xzScale(Z[i]))
    //   .attr("y", i => yScale(Y[i]))
    //   .attr("width", xzScale.bandwidth())
    //   .attr("height", i => yScale(0) - yScale(Y[i]))
    //   .attr("fill", i => zScale(Z[i]));








    this.svg.selectAll("text.bar")
      .data(this.data)
      .enter().append("text")
      .attr("class", "bar")
      .attr("text-anchor", "middle")
      .attr("x", function (d: any) { return x(d.Framework) })
      .attr("y", function (d: any) { return y(d.Stars) - 5; })
      .text(function (d: any) { return d.Stars; })
      .attr("transform", "translate(25,0)");
  }
}
