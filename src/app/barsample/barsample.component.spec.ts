import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarsampleComponent } from './barsample.component';

describe('BarsampleComponent', () => {
  let component: BarsampleComponent;
  let fixture: ComponentFixture<BarsampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarsampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarsampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
