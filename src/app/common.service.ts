import { Injectable } from '@angular/core';
import { apis } from './config/api';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  apiRes: any;
  selectedSerialNo: any;
  palletId: any;
  masterApiData: any;
  constructor(private httpService: HttpService) { }

  getImages() {
    return new Promise((resolve, reject) => {
      this.httpService.get(apis.imagesAPI).subscribe((res) => {
        this.apiRes = res;
        typeof (res);
        console.log(res, "json")
        if (res) {
          resolve(res);
        }

      })
    })
  }
  getMasterData() {
    console.log(apis.masterAPI)
    this.selectedSerialNo = localStorage.getItem('serialNo')
    return new Promise((resolve, reject) => {

      let queryParams = '';
      queryParams += '?Serial No=' + this.selectedSerialNo
      this.httpService.get(apis.masterAPI + queryParams).subscribe((res: any) => {
        console.log(res, "masster");
        if (res) {
          resolve(res);
        }
      })
    })
  }

  getDecals() {
    this.selectedSerialNo = localStorage.getItem('serialNo')

    return new Promise((resolve, reject) => {
      let queryParams = '';
      queryParams += '?Serial No=' + this.selectedSerialNo
      this.httpService.get(apis.decalsAPI + queryParams).subscribe((res: any) => {
        console.log(res);
        if (res) {
          resolve(res);
        }
      })
    })
  }
  getLeftRight() {
    return new Promise((resolve, reject) => {
      let queryParams = '';
      queryParams += '?Serial No=' + this.selectedSerialNo
      this.httpService.get(apis.leftRightAPI + queryParams).subscribe((res: any) => {
        console.log(res);
        if (res) {
          resolve(res);
        }
      })
    })
  }
  getbboximages() {
    return new Promise((resolve, reject) => {
      let queryParams = '';
      queryParams += '?Serial No=' + this.selectedSerialNo
      this.httpService.get(apis.imagesAPI + queryParams).subscribe((res: any) => {
        console.log(res);
        if (res) {
          resolve(res);
        }
      })
    })
  }
}
