import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as pbi from 'powerbi-client';

@Component({
  selector: 'app-analytics-data',
  templateUrl: './analytics-data.component.html',
  styleUrls: ['./analytics-data.component.scss']
})
export class AnalyticsDataComponent implements OnInit {
  // public screenHeight: number | undefined;
  // @ViewChild('reportContainer') reportContainer: ElementRef | undefined;
  reportURL: any = 'https://app.powerbi.com/reportEmbed?reportId=f226a495-2995-4851-bead-237aa9660391&autoAuth=true&ctid=432a4219-1a46-4b7f-92ce-aae7bc705c26';

  constructor() { }

  ngOnInit(): void {
    // this.showReport();
  }
  // showReport() {
  //   // Report's Secured Token
  //   let accessToken = '<Your Access Token Key1>  ';

  //   // Embed URL
  //   let embedUrl = 'https://app.powerbi.com/reportEmbed?reportId=9e8d9020-c873-4d1e-b699-ec48ed51cd9c&autoAuth=true&ctid=432a4219-1a46-4b7f-92ce-aae7bc705c26&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLWluZGlhLXdlc3QtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D';

  //   // Report ID
  //   let embedReportId = '9e8d9020-c873-4d1e-b699-ec48ed51cd9c&autoAuth=true&ctid=432a4219-1a46-4b7f-92ce-aae7bc705c26&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLWluZGlhLXdlc3QtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D';

  //   // Define a Configuration used to describe the what and how to embed.
  //   // This object is used when calling powerbi.embed.
  //   // This also includes settings and options such as filters.

  //   let config = {
  //     type: 'report',
  //     accessToken: accessToken,
  //     embedUrl: embedUrl,
  //     id: embedReportId,
  //     settings: {
  //       filterPaneEnabled: true,
  //       navContentPaneEnabled: true
  //     }
  //   };

  //   // Grab the reference to the div HTML element that will host the report.
  //   let reportContainer = <HTMLElement>document.getElementById('reportContainer');

  //   // Embed the report and display it within the div container.
  //   let powerbi = new pbi.service.Service(pbi.factories.hpmFactory, pbi.factories.wpmpFactory, pbi.factories.routerFactory);
  //   let report = powerbi.embed(reportContainer, config);

  //   // Report.off removes a given event handler if it exists.
  //   report.off("loaded");

  //   // Report.on will add an event handler which prints to Log window.
  //   report.on("loaded", function () {
  //     console.log("Loaded");
  //   });
  // }

}
