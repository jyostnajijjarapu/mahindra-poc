import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { apis } from '../config/api';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // data: any = [

  //   { model_left: 'yes', decal_left: 'yes', decal_right: 'yes', model_right: 'yes', pass: 'passed', model_match: 'yes', decal_match: 'yes' }

  // ];
  // tableData = {
  //   "Failure": { "DECAL": "Matched", "Model Name": "Matched" },
  //   "Left": { "DECAL": "BHM", "Model Name": "Mahindra 575 DI" },
  //   "Master Status": { "Message": "NO MISMATCHES", "Status": "Passed" },
  //   "Right": { "DECAL": "BHM", "Model Name": "Mahindra 575 DI" }
  // }
  data: any = [
    { serialNo: 40321, 'batch': 4500002417, 'line': 'D1', 'serial': 40321, 'chasis': 'ZF1ACJF00439' },
    { serialNo: 40322, 'batch': 4500002418, 'line': 'D1', 'serial': 40322, 'chasis': 'ZF1ACJF00440' },
    { serialNo: 40323, 'batch': 4500002419, 'line': 'D1', 'serial': 40323, 'chasis': 'ZF1ACJF00441' },
    { serialNo: 40324, 'batch': 4500002420, 'line': 'D1', 'serial': 40324, 'chasis': 'ZF1ACJF00442' },
  ]
  // imageData = {
  //   "Left": "https://mahindra.file.core.windows.net/tractor/BBOX/left.jpg?sv=2021-06-08&ss=bfqt&srt=sco&sp=rtfx&se=2022-06-29T21:20:09Z&st=2022-06-20T13:20:09Z&spr=https&sig=p7ZU%2FUHPiIINnkL9yH%2BE6040DrthYU5MWXbwwbOjlLk%3D",
  //   "Right": "https://mahindra.file.core.windows.net/tractor/BBOX/right.jpg?sv=2021-06-08&ss=bfqt&srt=sco&sp=rtfx&se=2022-06-29T21:20:09Z&st=2022-06-20T13:20:09Z&spr=https&sig=p7ZU%2FUHPiIINnkL9yH%2BE6040DrthYU5MWXbwwbOjlLk%3D"
  // }
  tableData: any;
  imageData: any;
  decalimage: any;
  batch: any;
  line: any;
  serial: any;
  chasis: any;
  constructor(private common: CommonService, private httpService: HttpService, private router: Router) { }

  async ngOnInit() {
    this.tableData = this.common.masterApiData;
    this.getDecals();
    this.getBboximages();
    let serialnum = localStorage.getItem('serialNo')
    for (let i = 0; i < this.data.length; i++) {
      if (serialnum == this.data[i].serialNo) {
        this.batch = this.data[i].batch;
        this.line = this.data[i].line;
        this.serial = this.data[i].serial;
        this.chasis = this.data[i].chasis;
      }

    }
    // console.log(this.imageData, "iiiiiiiiiii")
    // console.log(this.tableData['Failure']['DECAL'])
    // console.log(this.tableData['Failure']['Model Name'])


  }
  getDecals() {
    this.common.getDecals().then((res: any) => {
      console.log(Object.keys(res).length, "hhhhhhhhhhhhhhhhhhhhhhhhh")
      if (Object.keys(res).length) {
        this.decalimage = res;

      }
    })
  }
  getBboximages() {
    let queryParams = '';
    queryParams += '?Serial No=' + this.common.selectedSerialNo
    this.httpService.get(apis.imagesAPI + queryParams).subscribe((res: any) => {
      console.log(res);
      if (Object.keys(res).length) {
        this.imageData = res
      }
    })
  }
  loadAnalytics() {
    this.router.navigate(['/home/analytics'])
  }
}
