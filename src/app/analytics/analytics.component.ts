import { Component, OnInit, ViewChild } from '@angular/core';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  shifts = ['All', 'Shift A', 'Shift B', 'Shift C'];
  // counts = ['Pass', 'Fail', 'Total'];
  // counts = ['Pass', 'Fail', 'Total'];
  @ViewChild('select')
  select!: MatSelect;

  allSelected = false;


  data1 = [
    { "Framework": "Mahindra 275 DI TU", "Stars": "9", },
    { "Framework": "Mahindra 475 DI XP Plus", "Stars": "10", },
    { "Framework": "Mahindra JIVO 245 DI", "Stars": "5", },
    { "Framework": "Mahindra 575 DI XP Plus", "Stars": "8", },

  ];

  data2 = [
    { "Framework": "Mahindra 275 DI TU", "Stars": "6", },
    { "Framework": "Mahindra 475 DI XP Plus", "Stars": "4", },
    { "Framework": "Mahindra JIVO 245 DI", "Stars": "7", },
    { "Framework": "Mahindra 575 DI XP Plus", "Stars": "3", },

  ]
  foods: any[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];
  constructor() { }

  ngOnInit(): void {
  }
  onShiftSelection(event: any) {
    if (event.value == 'All') {
      this.allSelected = true;
    }
  }
  onCountSelection(event: any) {

  }
  toggleAllSelection() {
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => item.deselect());
    }
  }
  optionClick() {
    let newStatus = true;
    this.select.options.forEach((item: MatOption) => {
      if (!item.selected) {
        newStatus = false;
      }
    });
    this.allSelected = newStatus;
  }
}
