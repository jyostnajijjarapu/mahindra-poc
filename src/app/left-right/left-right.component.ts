import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-left-right',
  templateUrl: './left-right.component.html',
  styleUrls: ['./left-right.component.scss']
})
export class LeftRightComponent implements OnInit {

  leftRightimgs: any;
  isLoading = false;

  constructor(private router: Router, private common: CommonService) { }

  ngOnInit() {
    console.log("img")

    this.common.getLeftRight().then((res: any) => {
      if (Object.keys(res).length) {
        this.leftRightimgs = res;

      }
      console.log(res);
    })

  }
  palletloading() {
    this.router.navigate(['/home/loader'])
  }

}
