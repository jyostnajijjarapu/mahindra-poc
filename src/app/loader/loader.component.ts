import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  loading = false;
  constructor(private router: Router, private common: CommonService) { }

  ngOnInit() {
    this.loading = true;
    this.common.getMasterData().then((res: any) => {
      console.log(res, "tytyyyyyy");
      this.common.masterApiData = res;
      if (Object.keys(res).length) {
        this.loading = false;
        setTimeout(() => {
          this.router.navigate(['/home/dashboard']);
        }, 1000);
      }

    })
  }

}
