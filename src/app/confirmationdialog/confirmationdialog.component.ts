import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmationdialog',
  templateUrl: './confirmationdialog.component.html',
  styleUrls: ['./confirmationdialog.component.scss']
})
export class ConfirmationdialogComponent implements OnInit {

  constructor(private router: Router, private dialogRef: MatDialogRef<ConfirmationdialogComponent>) { }

  ngOnInit(): void {
  }
  onConfirmClick() {
    this.dialogRef.close(true);
    // if (true) {
    //   this.router.navigate(['/home/left-right'])

    // }
  }
}
