import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { apis } from '../config/api';
import { HttpService } from '../http.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';

@Component({
  selector: 'app-decal-page',
  templateUrl: './decal-page.component.html',
  styleUrls: ['./decal-page.component.scss']
})
export class DecalPageComponent implements OnInit {
  loadImages: any
  constructor(private common: CommonService, private router: Router, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.common.getDecals().then((res: any) => {
      if (Object.keys(res).length) {
        this.loadImages = res;
      }
    })
  }
  loadLeftRight() {
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: {
        message: 'Stickers Pasted?',
        buttonText: {
          ok: 'Yes',
          cancel: 'No'
        }
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.router.navigate(['/home/left-right'])

        const a = document.createElement('a');
        a.click();
        a.remove();

      }
    });
  }
}
