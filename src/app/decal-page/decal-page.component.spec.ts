import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecalPageComponent } from './decal-page.component';

describe('DecalPageComponent', () => {
  let component: DecalPageComponent;
  let fixture: ComponentFixture<DecalPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecalPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecalPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
