import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ModelSelectionComponent } from '../model-selection/model-selection.component';
import { MaterialModule } from '../material.module';
import { LeftRightComponent } from '../left-right/left-right.component';
import { LoaderComponent } from '../loader/loader.component';
import { AnalyticsComponent } from '../analytics/analytics.component';
import { BarchartComponent } from '../barchart/barchart.component';
import { DecalPageComponent } from '../decal-page/decal-page.component';
import { DonutComponent } from '../donut/donut.component';
import { BarsampleComponent } from '../barsample/barsample.component';
import { AnalyticsDataComponent } from '../analytics-data/analytics-data.component';
import { SafeUrlPipe } from 'src/app/pipes/safe-url.pipe';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ModelSelectionComponent,
    LeftRightComponent,
    LoaderComponent,
    AnalyticsComponent,
    BarchartComponent,
    DecalPageComponent,
    DonutComponent,
    BarsampleComponent,
    AnalyticsDataComponent, SafeUrlPipe, ConfirmationdialogComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule, MaterialModule, FormsModule, ReactiveFormsModule
  ]
})
export class HomeModule { }
