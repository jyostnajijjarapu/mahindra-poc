import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common.service';
import { apis } from 'src/app/config/api';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})
export class MainpageComponent implements OnInit {
  data: any = [
    { serialNo: 40321, 'batch': 4500002417, 'line': 'D1', 'serial': 40321, 'chasis': 'ZF1ACJF00439' },
    { serialNo: 40322, 'batch': 4500002418, 'line': 'D1', 'serial': 40322, 'chasis': 'ZF1ACJF00440' },
    { serialNo: 40323, 'batch': 4500002419, 'line': 'D1', 'serial': 40323, 'chasis': 'ZF1ACJF00441' },
    { serialNo: 40324, 'batch': 4500002420, 'line': 'D1', 'serial': 40324, 'chasis': 'ZF1ACJF00442' },
  ]
  tableData: any;
  imageData: any;
  decalimage: any;
  batch: any;
  line: any;
  serial: any;
  chasis: any;
  constructor(private common: CommonService, private httpService: HttpService, private router: Router) { }
  async ngOnInit() {
    this.tableData = this.common.masterApiData;
    this.getDecals();
    this.getBboximages();
    let serialnum = localStorage.getItem('serialNo')
    for (let i = 0; i < this.data.length; i++) {
      if (serialnum == this.data[i].serialNo) {
        this.batch = this.data[i].batch;
        this.line = this.data[i].line;
        this.serial = this.data[i].serial;
        this.chasis = this.data[i].chasis;
      }

    }
    // console.log(this.imageData, "iiiiiiiiiii")
    // console.log(this.tableData['Failure']['DECAL'])
    // console.log(this.tableData['Failure']['Model Name'])


  }
  getDecals() {
    this.common.getDecals().then((res: any) => {
      console.log(Object.keys(res).length, "hhhhhhhhhhhhhhhhhhhhhhhhh")
      if (Object.keys(res).length) {
        this.decalimage = res;

      }
    })
  }
  getBboximages() {
    let queryParams = '';
    queryParams += '?Serial No=' + this.common.selectedSerialNo
    this.httpService.get(apis.imagesAPI + queryParams).subscribe((res: any) => {
      console.log(res);
      if (Object.keys(res).length) {
        this.imageData = res
      }
    })
  }
  loadAnalytics() {
    this.router.navigate(['/home/analytics'])
  }
}
