export const menuItems = [
    {
        title: 'Dashboard',
        routerLink: 'dashboard',
        icon: 'dashboard',

    },
    {
        title: 'Analytics',
        routerLink: 'analytics',
        icon: 'insert_chart_outlined',

    },
    {
        title: 'Settings',
        routerLink: 'settings',
        icon: 'settings',
    },

]
