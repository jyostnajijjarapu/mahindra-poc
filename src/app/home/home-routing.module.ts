import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalyticsDataComponent } from '../analytics-data/analytics-data.component';
import { AnalyticsComponent } from '../analytics/analytics.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DecalPageComponent } from '../decal-page/decal-page.component';
import { LeftRightComponent } from '../left-right/left-right.component';
import { LoaderComponent } from '../loader/loader.component';
import { ModelSelectionComponent } from '../model-selection/model-selection.component';
import { HomeComponent } from './home.component';
import { MainpageComponent } from './mainpage/mainpage.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [

      { path: 'dashboard', component: MainpageComponent, data: { breadcrumb: 'Dashboard' } },
      { path: 'analytics', component: AnalyticsDataComponent, data: { breadcrumb: 'Analytics' } },
      { path: 'model-selection', component: ModelSelectionComponent, data: { breadcrumb: 'Enter Serial No' } },
      { path: 'left-right', component: LeftRightComponent, data: { breadcrumb: 'Captured images' } },
      { path: 'loader', component: LoaderComponent, data: { breadcrumb: 'Info Loading..' } },
      { path: 'decals', component: DecalPageComponent, data: { breadcrumb: 'What to be Sticked?' } }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
